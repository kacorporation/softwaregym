package com.example.demo.dto;

import com.example.demo.entity.Product;

public class ProductDTO {
    private String description;
    private Long cost;
    private Long stock;

    public ProductDTO() {
    }
    public ProductDTO(Product entity) {
        this.description = entity.getDescription();
        this.cost = entity.getCost();
        this.stock = entity.getStock();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
