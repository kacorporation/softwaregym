package com.example.demo.dto;

import com.example.demo.entity.User;

public class UserDTO {
    private String mail;
    private String password;

    public UserDTO() {
    }
    public UserDTO(User entity) {
        this.mail = entity.getMail();
        this.password = entity.getPassword();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
