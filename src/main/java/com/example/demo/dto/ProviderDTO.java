package com.example.demo.dto;

import com.example.demo.entity.Provider;
import java.util.Date;

public class ProviderDTO {
    private String businessName;
    private String locate;
    private Long phone;
    private String address;
    private String mail;
    private boolean active;
    private Date admissionDate;
    private Date egressDate;

    public ProviderDTO() {
    }
    public ProviderDTO(Provider entity) {
        this.businessName = entity.getBusinessName();
        this.locate = entity.getLocate();
        this.phone = entity.getPhone();
        this.address = entity.getAddress();
        this.mail = entity.getMail();
        this.active = entity.isActive();
        this.admissionDate = entity.getAdmissionDate();
        this.egressDate = entity.getEgressDate();
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getEgressDate() {
        return egressDate;
    }

    public void setEgressDate(Date egressDate) {
        this.egressDate = egressDate;
    }
}
