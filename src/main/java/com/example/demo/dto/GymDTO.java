package com.example.demo.dto;

import com.example.demo.entity.Gym;

public class GymDTO {
    private String businessName;
    private String locate;
    private String address;
    private Long cuit;
    private Long phone;


    public GymDTO(){}
    public GymDTO(Gym entity){
        this.businessName = entity.getBusinessName();
        this.locate = entity.getLocate();
        this.address = entity.getAddress();
        this.cuit = entity.getCuit();
        this.phone = entity.getPhone();
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCuit() {
        return cuit;
    }

    public void setCuit(Long cuit) {
        this.cuit = cuit;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }
}
