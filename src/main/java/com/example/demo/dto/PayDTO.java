package com.example.demo.dto;

import com.example.demo.entity.Pay;
import java.util.Date;

public class PayDTO {
    private Date datePay;
    private boolean isPay;

    public PayDTO() {
    }
    public PayDTO(Pay entity) {
        this.datePay = entity.getDatePay();
        this.isPay = entity.isPay();
    }

    public Date getDatePay() {
        return datePay;
    }

    public void setDatePay(Date datePay) {
        this.datePay = datePay;
    }

    public boolean isPay() {
        return isPay;
    }

    public void setPay(boolean pay) {
        isPay = pay;
    }
}
