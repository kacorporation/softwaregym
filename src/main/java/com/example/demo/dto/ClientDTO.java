package com.example.demo.dto;

import com.example.demo.entity.Client;
import java.util.Date;
import java.util.List;

public class ClientDTO {
    private Long id;
    private String desciption;
    private Long dni;
    private String name;
    private String lastname;
    //private List<GymDTO> gyms;
    private String genre;
    private Long phone;
    private boolean isActive;
    private boolean medicalCoverage;
    private String address;
    private Date admissionDate;
    private Date egressDate;
    private Date birthDate;
    private String mail;

    public ClientDTO() {
    }

    public ClientDTO(Client entity){
        this.dni = entity.getDni();
        this.name = entity.getName();
        this.lastname = entity.getLastname();
        this.genre = entity.getGenre();
        this.phone = entity.getPhone();
        this.isActive = entity.isActive();
        this.medicalCoverage = entity.isMedicalCoverage();
        this.address = entity.getAddress();
        this.admissionDate = entity.getAdmissionDate();
        this.egressDate = entity.getEgressDate();
        this.birthDate = entity.getBirthDate();
        this.mail = entity.getMail();
    }


    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /*public List<GymDTO> getGyms() {
        return gyms;
    }

    public void setGyms(List<GymDTO> gyms) {
        this.gyms = gyms;
    }*/

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isMedicalCoverage() {
        return medicalCoverage;
    }

    public void setMedicalCoverage(boolean medicalCoverage) {
        this.medicalCoverage = medicalCoverage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getEgressDate() {
        return egressDate;
    }

    public void setEgressDate(Date egressDate) {
        this.egressDate = egressDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
