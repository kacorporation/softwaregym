package com.example.demo.utils;

// Esta clase va a tener que tener un objeto para devolver
//y un message tiene que ser string
//Response va a ser Objetc
public class ResponseDTO {
    private Object response;
    private String message;

    public ResponseDTO() {
    }


    public ResponseDTO(Object response, String message) {
        this.response = response;
        this.message= message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }
}
