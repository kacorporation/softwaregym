package com.example.demo.service;

import com.example.demo.dto.ProductDTO;
import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService{
    private final ProductRepository repository;
    @Autowired
    public ProductServiceImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public Product postProduct(ProductDTO productDTO) {
        return repository.save(new Product(productDTO));
    }

    @Override
    public List<ProductDTO> getProductDTOAll() {
        return repository.findAll()
                .stream()
                .map(producto -> new ProductDTO(producto))
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO getProductDTOById(long id) {
        if(repository.findById(id).isPresent()){
            return new ProductDTO(repository.findById(id).get());
        }else{
            return null;
        }
    }
}
