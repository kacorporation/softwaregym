package com.example.demo.service;

import com.example.demo.dto.EmployeeDTO;
import com.example.demo.entity.Employee;
import java.util.List;


public interface EmployeeService {
    public Employee postEmployee(EmployeeDTO employeeDTO);
    public List<EmployeeDTO> getEmployeeDTOAll();
    public EmployeeDTO getEmployeeDTOById(long id);
}
