package com.example.demo.service;

import com.example.demo.dto.ProductDTO;
import com.example.demo.entity.Product;
import java.util.List;

public interface ProductService {
    public Product postProduct(ProductDTO productDTO);
    public List<ProductDTO> getProductDTOAll();
    public ProductDTO getProductDTOById(long id);
}
