package com.example.demo.service;

import com.example.demo.dto.PayDTO;
import com.example.demo.entity.Pay;
import java.util.List;

public interface PayService {
    public Pay postPay(PayDTO payDTO);
    public List<PayDTO> getPayDTOAll();
    public PayDTO getPayDTOById(long id);
}
