package com.example.demo.service;

import com.example.demo.dto.GymDTO;
import com.example.demo.entity.Gym;
import com.example.demo.repository.GymRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GymServiceImpl implements GymService{
    private final GymRepository repository;
    @Autowired
    public GymServiceImpl(GymRepository repository) {
        this.repository = repository;
    }

    @Override
    public Gym postGym(GymDTO gymDTO) {
        return repository.save(new Gym(gymDTO));
    }

    @Override
    public List<GymDTO> getGymDTOAll() {
        return repository.findAll()
                .stream()
                .map(gimnasio -> new GymDTO(gimnasio))
                .collect(Collectors.toList());
    }

    @Override
    public GymDTO getGymDTOById(long id) {
        if(repository.findById(id).isPresent()){
            return new GymDTO(repository.findById(id).get());
        }else{
            return null;
        }
    }
}
