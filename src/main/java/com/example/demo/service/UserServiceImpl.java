package com.example.demo.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{
    private final UserRepository repository;
    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User postUser(UserDTO userDTO) {
        return repository.save(new User(userDTO));
    }

    @Override
    public List<UserDTO> getUserDTOAll() {
        return repository.findAll()
                .stream()
                .map(usuario -> new UserDTO(usuario))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getUserDTOById(long id) {
        if(repository.findById(id).isPresent()){
            return new UserDTO(repository.findById(id).get());
        }else{
            return null;
        }
    }
}
