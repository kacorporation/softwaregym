package com.example.demo.service;

import com.example.demo.dto.ClientDTO;
import com.example.demo.entity.Client;
import java.util.List;

public interface ClientService {
    public Client postClientDTO(ClientDTO clientDTO);
    public List<ClientDTO> getClientDTOAll();
    public ClientDTO getClientDTOById(long id);
}
