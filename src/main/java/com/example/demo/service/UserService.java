package com.example.demo.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import java.util.List;

public interface UserService {
    public User postUser(UserDTO userDTO);
    public List<UserDTO> getUserDTOAll();
    public UserDTO getUserDTOById(long id);
}
