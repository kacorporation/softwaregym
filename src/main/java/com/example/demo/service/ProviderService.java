package com.example.demo.service;

import com.example.demo.dto.ProviderDTO;
import com.example.demo.entity.Provider;
import java.util.List;

public interface ProviderService {
    public Provider postProvider(ProviderDTO providerDTO);
    public List<ProviderDTO> getProviderDTOAll();
    public ProviderDTO getProviderDTOById(long id);
}
