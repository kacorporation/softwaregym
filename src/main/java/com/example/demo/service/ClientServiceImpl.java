package com.example.demo.service;

import com.example.demo.entity.Client;
import com.example.demo.entity.ClientType;
import com.example.demo.repository.ClientRepository;
import com.example.demo.dto.ClientDTO;
import com.example.demo.repository.ClientTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ClientTypeRepository clientTypeRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository, ClientTypeRepository clientTypeRepository) {
        this.clientRepository = clientRepository;
        this.clientTypeRepository = clientTypeRepository;
    }



    @Override
    public Client postClientDTO(ClientDTO clientDTO) {
        Client client = new Client(clientDTO);
        List<ClientType> clientTypes = clientTypeRepository.findAll();
        for(int i=0;i<clientTypes.size();i++) {
            //ClientType clientType = clientTypeRepository.findById((long)i).get();
            ClientType clientType = clientTypes.get(i);
            if (clientDTO.getDesciption().equals(clientType.getDescription())){
                //client.setClientTypeId((long)i);
                client.setClientTypeId(clientType.getId());
            }
        }
        return clientRepository.save(client);
    }

    @Override
    public List<ClientDTO> getClientDTOAll() {
        List<ClientDTO> clientDTOS = new ArrayList<>();
        List<Client> clients = clientRepository.findAll();
        for(int i=0;i<clients.size();i++) {
            //Client client = clientRepository.findById((long) i).get();
            Client client = clients.get(i);
            ClientType clientType = clientTypeRepository.findById(client.getClientTypeId()).get();
            ClientDTO clientDTO = new ClientDTO(client);
            clientDTO.setDesciption(clientType.getDescription());
            clientDTOS.add(clientDTO);
        }
        return clientDTOS;
    }
   @Override
    public ClientDTO getClientDTOById(long id) {
       if(clientRepository.findById(id).isPresent()) {
           Client client = clientRepository.findById(id).get();
           ClientType clientType = clientTypeRepository.findById(client.getClientTypeId()).get();
           ClientDTO clientDTO = new ClientDTO(client);
           clientDTO.setDesciption(clientType.getDescription());
           return clientDTO;
       }else {
           return null;
       }
   }
}
