package com.example.demo.service;

import com.example.demo.dto.ProviderDTO;
import com.example.demo.entity.Provider;
import com.example.demo.repository.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProviderServiceImpl implements ProviderService {
    private final ProviderRepository repository;
    @Autowired
    public ProviderServiceImpl(ProviderRepository repository) {
        this.repository = repository;
    }

    @Override
    public Provider postProvider(ProviderDTO providerDTO) {
        return repository.save(new Provider(providerDTO));
    }

    @Override
    public List<ProviderDTO> getProviderDTOAll() {
        return repository.findAll()
                .stream()
                .map(proveedor -> new ProviderDTO(proveedor))
                .collect(Collectors.toList());
    }

    @Override
    public ProviderDTO getProviderDTOById(long id) {
        if (repository.findById(id).isPresent()){
            return new ProviderDTO(repository.findById(id).get());
        }else{
            return null;
        }
    }
}
