package com.example.demo.service;

import com.example.demo.dto.GymDTO;
import com.example.demo.entity.Gym;
import java.util.List;

public interface GymService {
    public Gym postGym(GymDTO gymDTO);
    public List<GymDTO> getGymDTOAll();
    public GymDTO getGymDTOById(long id);
}
