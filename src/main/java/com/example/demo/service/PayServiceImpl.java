package com.example.demo.service;

import com.example.demo.dto.PayDTO;
import com.example.demo.entity.Pay;
import com.example.demo.repository.PayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PayServiceImpl implements PayService{
    private final PayRepository repository;
    @Autowired
    public PayServiceImpl(PayRepository repository) {
        this.repository = repository;
    }

    @Override
    public Pay postPay(PayDTO payDTO) {
        return repository.save(new Pay(payDTO));
    }

    @Override
    public List<PayDTO> getPayDTOAll() {
        return repository.findAll()
                .stream()
                .map(pago -> new PayDTO(pago))
                .collect(Collectors.toList());
    }

    @Override
    public PayDTO getPayDTOById(long id) {
        if(repository.findById(id).isPresent()){
            return new PayDTO(repository.findById(id).get());
        }else{
            return null;
        }
    }
}
