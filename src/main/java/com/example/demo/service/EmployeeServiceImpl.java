package com.example.demo.service;


import com.example.demo.dto.EmployeeDTO;
import com.example.demo.entity.Employee;
import com.example.demo.entity.EmployeeType;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.EmployeeTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{
    private final EmployeeRepository employeeRepository;
    private final EmployeeTypeRepository employeeTypeRepository;
    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeTypeRepository employeeTypeRepository) {
        this.employeeRepository = employeeRepository;
        this.employeeTypeRepository = employeeTypeRepository;
    }

    @Override
    public Employee postEmployee(EmployeeDTO employeeDTO) {
        Employee employee = new Employee(employeeDTO);
        List<EmployeeType> employeeTypes = employeeTypeRepository.findAll();
        for(int i=0;i<employeeTypes.size();i++) {
            EmployeeType employeeType = employeeTypes.get(i);
            if (employeeDTO.getDescription().equals(employeeType.getDescription())){
                employee.setEmployeeTypeId(employeeType.getId());
            }
        }
        return employeeRepository.save(employee);
    }

    @Override
    public List<EmployeeDTO> getEmployeeDTOAll() {
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        List<Employee> employees = employeeRepository.findAll();
        for(int i=0;i<employees.size();i++) {
            Employee employee = employees.get(i);
            EmployeeType employeeType = employeeTypeRepository.findById(employee.getEmployeeTypeId()).get();
            EmployeeDTO employeeDTO = new EmployeeDTO(employee);
            employeeDTO.setDescription(employeeType.getDescription());
            employeeDTOS.add(employeeDTO);
        }
        return employeeDTOS;
    }
    @Override
    public EmployeeDTO getEmployeeDTOById(long id) {
        if(employeeRepository.findById(id).isPresent()) {
            Employee client = employeeRepository.findById(id).get();
            EmployeeType employeeType = employeeTypeRepository.findById(client.getEmployeeTypeId()).get();
            EmployeeDTO employeeDTO = new EmployeeDTO(client);
            employeeDTO.setDescription(employeeType.getDescription());
            return employeeDTO;
        }else {
            return null;
        }
    }
}