package com.example.demo.controller;

import com.example.demo.dto.UserDTO;
import com.example.demo.service.UserService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    final UserService service;
    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postUser(@RequestBody UserDTO userDTO){
       return new ResponseEntity<>(new ResponseDTO(service.postUser(userDTO),
               "CREATED_OK"), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<ResponseDTO> getUserDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getUserDTOAll(),
                "ACCEPTED_OK"), HttpStatus.ACCEPTED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO> getUserDTOById(@PathVariable long id){
        if(service.getUserDTOById(id) != null) {
            return new ResponseEntity<>(new ResponseDTO(service.getUserDTOById(id),
                    "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getUserDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
