package com.example.demo.controller;

import com.example.demo.dto.ProductDTO;
import com.example.demo.service.ProductService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/products")
public class ProductController {
    final ProductService service;
    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postProduct(@RequestBody ProductDTO productDTO){
        return new ResponseEntity<>(new ResponseDTO(service.postProduct(productDTO),
                "CREATED_OK"), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<ResponseDTO> getProductDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getProductDTOAll(),
                "ACCEPTED_OK"), HttpStatus.ACCEPTED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO> getProductDTOById(@PathVariable long id){
        if(service.getProductDTOById(id) != null) {
            return new ResponseEntity<>(new ResponseDTO(service.getProductDTOById(id),
                    "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getProductDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
