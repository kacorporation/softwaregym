package com.example.demo.controller;

import com.example.demo.dto.GymDTO;
import com.example.demo.service.GymService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/gyms")
public class GymController {
    final GymService service;
    @Autowired
    public GymController(GymService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postGym(@RequestBody GymDTO gymDTO){
        return new ResponseEntity<>(new ResponseDTO(service.postGym(gymDTO),
                "CREATED_OK"), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<ResponseDTO> getGymDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getGymDTOAll(),
                "ACCEPTED_OK"), HttpStatus.ACCEPTED);
    }
    @GetMapping ("/{id}")
    public ResponseEntity<ResponseDTO> getGymDTOById(@PathVariable long id){
        if(service.getGymDTOById(id) != null){
        return new ResponseEntity<>(new ResponseDTO(service.getGymDTOById(id),
                "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getGymDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
