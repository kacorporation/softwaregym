package com.example.demo.controller;

import com.example.demo.dto.EmployeeDTO;
import com.example.demo.service.EmployeeService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController//url de nuesta peticion va en plural por convencion de las api rest
@RequestMapping("/employees")
public class EmployeeController {
    final EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postEmployee(@RequestBody EmployeeDTO employeeDTO){
        return new ResponseEntity<>(new ResponseDTO(service.postEmployee(employeeDTO),
                "CREATED_OK"), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<ResponseDTO> getEmployeeDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getEmployeeDTOAll(),
                "ACCEPTED_OK"),HttpStatus.ACCEPTED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO>getEmployeeDTOById(@PathVariable long id) {
        if(service.getEmployeeDTOById(id) != null) {
            return new ResponseEntity<>(new ResponseDTO(service.getEmployeeDTOById(id),
                    "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getEmployeeDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
