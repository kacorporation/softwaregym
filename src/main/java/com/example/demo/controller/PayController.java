package com.example.demo.controller;


import com.example.demo.dto.PayDTO;
import com.example.demo.service.PayService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/pays")
public class PayController {
    final PayService service;
    @Autowired
    public PayController(PayService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postPay(@RequestBody PayDTO payDTO){
        return new ResponseEntity<>(new ResponseDTO(service.postPay(payDTO),
                "CREATED_OK"), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<ResponseDTO> getPayDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getPayDTOAll(),
                "ACCEPTED_OK"), HttpStatus.ACCEPTED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO> getPayDTOById(@PathVariable long id){
        if(service.getPayDTOById(id) != null){
        return new ResponseEntity<>(new ResponseDTO(service.getPayDTOById(id),
                "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getPayDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
