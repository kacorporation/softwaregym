package com.example.demo.controller;

import com.example.demo.dto.ClientDTO;
import com.example.demo.service.ClientService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping ("/clients")
public class ClientController {
    final ClientService service;

    @Autowired
    public ClientController(ClientService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postClient(@RequestBody ClientDTO clientDTO){
        return new ResponseEntity<>(new ResponseDTO(service.postClientDTO(clientDTO),
                "CREATED_OK"),HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<ResponseDTO> getClientDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getClientDTOAll(),
                "ACCEPTED_OK"),HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO> getClientDTOById(@PathVariable long id) {
        if(service.getClientDTOById(id) != null) {
            return new ResponseEntity<>(new ResponseDTO(service.getClientDTOById(id),
                    "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getClientDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
