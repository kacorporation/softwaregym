package com.example.demo.controller;

import com.example.demo.dto.ProviderDTO;
import com.example.demo.service.ProviderService;
import com.example.demo.utils.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/providers")
public class ProviderController {
    final ProviderService service;
    @Autowired
    public ProviderController(ProviderService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> postProvider(@RequestBody ProviderDTO providerDTO){
        return new ResponseEntity<>(new ResponseDTO(service.postProvider(providerDTO),
                "CREATED_OK"), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<ResponseDTO> getProviderDTOAll(){
        return new ResponseEntity<>(new ResponseDTO(service.getProviderDTOAll(),
                "ACCEPTED_OK"),HttpStatus.ACCEPTED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO> getProviderDTOById(@PathVariable long id){
        if(service.getProviderDTOById(id) != null) {
            return new ResponseEntity<>(new ResponseDTO(service.getProviderDTOById(id),
                    "FOUND_OK"), HttpStatus.FOUND);
        }else{
            return new ResponseEntity<>(new ResponseDTO(service.getProviderDTOById(id),
                    "NOT_FOUND"), HttpStatus.NOT_FOUND);
        }
    }
}
