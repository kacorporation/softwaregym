package com.example.demo.repository;

import com.example.demo.entity.GymEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GymEmployeeRepository extends JpaRepository<GymEmployee,Long> {
}
