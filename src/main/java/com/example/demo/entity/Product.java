package com.example.demo.entity;

import com.example.demo.dto.ProductDTO;
import javax.persistence.*;

@Entity(name = "producto")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "descripcion")
    private String description;
    @Column(name = "precio")
    private Long cost;
    @Column(name = "stock")
    private Long stock;

    public Product() {
    }

    public Product(ProductDTO productDTO) {
        this.description = productDTO.getDescription();
        this.cost = productDTO.getCost();
        this.stock = productDTO.getStock();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
