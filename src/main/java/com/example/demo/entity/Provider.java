package com.example.demo.entity;

import com.example.demo.dto.ProviderDTO;
import javax.persistence.*;
import java.util.Date;

@Entity(name = "proveedor")
public class Provider {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "razon_social")
    private String businessName;
    @Column(name = "localidad")
    private String locate;
    @Column(name = "telefono")
    private Long phone;
    @Column(name = "direccion")
    private String address;
    @Column(name = "mail")
    private String mail;
    @Column(name = "es_activo")
    private boolean active;
    @Column(name = "fecha_ingreso")
    private Date admissionDate;
    @Column(name = "fecha_egreso")
    private Date egressDate;

    public Provider() {
    }

    public Provider(ProviderDTO providerDTO) {
        this.businessName = providerDTO.getBusinessName();
        this.locate = providerDTO.getLocate();
        this.phone = providerDTO.getPhone();
        this.address = providerDTO.getAddress();
        this.mail = providerDTO.getMail();
        this.active = providerDTO.isActive();
        this.admissionDate = providerDTO.getAdmissionDate();
        this.egressDate = providerDTO.getEgressDate();
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getEgressDate() {
        return egressDate;
    }

    public void setEgressDate(Date egressDate) {
        this.egressDate = egressDate;
    }
}
