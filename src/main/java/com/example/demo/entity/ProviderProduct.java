package com.example.demo.entity;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "proveedor_producto")
public class ProviderProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "producto_id")
    private Long productId;
    @Column(name = "proveedor_id")
    private Long providerId;
    @Column(name = "fecha")
    private Date date;

    public ProviderProduct() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
