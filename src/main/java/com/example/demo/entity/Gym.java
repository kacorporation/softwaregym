package com.example.demo.entity;

import com.example.demo.dto.GymDTO;
import javax.persistence.*;

@Entity(name = "gimnasio")
public class Gym {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "razon_social")
    private String businessName;
    @Column(name = "localidad")
    private String locate;
    @Column(name = "direccion")
    private String address;
    @Column(name = "cuit")
    private Long cuit;
    @Column(name = "telefono")
    private Long phone;

    public Gym() {
    }

    public Gym(GymDTO gymDTO){
        this.businessName = gymDTO.getBusinessName();
        this.locate = gymDTO.getLocate();
        this.address = gymDTO.getAddress();
        this.cuit = gymDTO.getCuit();
        this.phone = gymDTO.getPhone();
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCuit() {
        return cuit;
    }

    public void setCuit(Long cuit) {
        this.cuit = cuit;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }
}
