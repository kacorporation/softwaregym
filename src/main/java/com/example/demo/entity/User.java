package com.example.demo.entity;

import com.example.demo.dto.UserDTO;
import javax.persistence.*;

@Entity(name = "usuario")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "mail")
    private String mail;
    @Column(name = "contraseña")
    private String password;

    public User() {
    }

    public User(UserDTO userDTO) {
        this.mail = userDTO.getMail();
        this.password = userDTO.getPassword();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
