package com.example.demo.entity;

import javax.persistence.*;

@Entity(name = "gimnasio_producto")
public class GymProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "producto_id")
    private Long productId;
    @Column(name = "gimnasio_id")
    private Long gymId;

    public GymProduct() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getGymId() {
        return gymId;
    }

    public void setGymId(Long gymId) {
        this.gymId = gymId;
    }
}
