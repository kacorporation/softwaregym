package com.example.demo.entity;

import com.example.demo.dto.PayDTO;
import javax.persistence.*;
import java.util.Date;

@Entity(name = "pago")
public class Pay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "fecha")
    private Date datePay;
    @Column(name = "pagado")
    private boolean isPay;

    public Pay() {
    }

    public Pay(PayDTO payDTO) {
        this.datePay = payDTO.getDatePay();
        this.isPay = payDTO.isPay();
    }

    public Date getDatePay() {
        return datePay;
    }

    public void setDatePay(Date datePay) {
        this.datePay = datePay;
    }

    public boolean isPay() {
        return isPay;
    }

    public void setPay(boolean pay) {
        isPay = pay;
    }
}
