package com.example.demo.entity;

import com.example.demo.dto.EmployeeDTO;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "trabajador")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dni")
    private Long dni;
    @Column(name = "nombre")
    private String name;
    @Column(name = "apellido")
    private String lastname;
    @Column(name = "genero")
    private String genre;
    @Column(name = "telefono")
    private Long phone;
    @Column(name = "es_activo")
    private boolean isActive;
    @Column(name = "direccion")
    private String address;
    @Column(name = "fecha_ingreso")
    private Date admissionDate;
    @Column(name = "fecha_egreso")
    private Date egressDate;
    @Column(name = "mail")
    private String mail;
    @Column(name = "fecha_nacimiento")
    private Date birthDate;
    @Column(name = "tipo_trabajador_id")
    private Long employeeTypeId;


    public Employee() {
    }

    public Employee(EmployeeDTO employeeDTO){
        this.dni = employeeDTO.getDni();
        this.name = employeeDTO.getName();
        this.lastname = employeeDTO.getLastname();
        this.genre = employeeDTO.getGenre();
        this.phone = employeeDTO.getPhone();
        this.isActive = employeeDTO.isActive();
        this.address = employeeDTO.getAddress();
        this.admissionDate = employeeDTO.getAdmissionDate();
        this.egressDate = employeeDTO.getEgressDate();
        this.birthDate = employeeDTO.getBirthDate();
        this.mail = employeeDTO.getMail();
    }

    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getEgressDate() {
        return egressDate;
    }

    public void setEgressDate(Date egressDate) {
        this.egressDate = egressDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Long getEmployeeTypeId() {
        return employeeTypeId;
    }

    public void setEmployeeTypeId(Long employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }
}