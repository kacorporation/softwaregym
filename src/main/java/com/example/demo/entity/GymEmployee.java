package com.example.demo.entity;

import javax.persistence.*;

@Entity(name = "gimnasio_trabajador")
public class GymEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "gimnasio_id")
    private Long gymId;
    @Column(name = "trabajador_id")
    private Long employeeId;

    public GymEmployee() {
    }

    public Long getGymId() {
        return gymId;
    }

    public void setGymId(Long gymId) {
        this.gymId = gymId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
}
