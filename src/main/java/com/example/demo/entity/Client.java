package com.example.demo.entity;

import com.example.demo.dto.ClientDTO;
import javax.persistence.*;
import java.util.Date;


@Entity(name = "cliente")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dni")
    private Long dni;
    @Column(name = "nombre")
    private String name;
    @Column(name= "apellido")
    private String lastname;
    @Column(name = "genero")
    private String genre;
    @Column(name = "telefono")
    private Long phone;
    @Column(name = "es_activo")
    private boolean isActive;
    @Column(name = "obra_social")
    private boolean medicalCoverage;
    @Column(name = "direccion")
    private String address;
    @Column(name = "fecha_ingreso")
    private Date admissionDate;
    @Column(name = "fecha_egreso")
    private Date egressDate;
    @Column(name = "fecha_nacimiento")
    private Date birthDate;
    @Column(name = "mail")
    private String mail;
    @Column(name = "tipo_cliente_id")
    private Long clientTypeId;

    public Client() {
    }

    public Client(ClientDTO clientDTO){
        this.dni = clientDTO.getDni();
        this.name = clientDTO.getName();
        this.lastname = clientDTO.getLastname();
        this.genre = clientDTO.getGenre();
        this.phone = clientDTO.getPhone();
        this.isActive = clientDTO.isActive();
        this.medicalCoverage = clientDTO.isMedicalCoverage();
        this.address = clientDTO.getAddress();
        this.admissionDate = clientDTO.getAdmissionDate();
        this.egressDate = clientDTO.getEgressDate();
        this.birthDate = clientDTO.getBirthDate();
        this.mail = clientDTO.getMail();
    }


    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isMedicalCoverage() {
        return medicalCoverage;
    }

    public void setMedicalCoverage(boolean medicalCoverage) {
        this.medicalCoverage = medicalCoverage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getEgressDate() {
        return egressDate;
    }

    public void setEgressDate(Date egressDate) {
        this.egressDate = egressDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Long getClientTypeId() {
        return clientTypeId;
    }

    public void setClientTypeId(Long clientTypeId) {
        this.clientTypeId = clientTypeId;
    }
}
