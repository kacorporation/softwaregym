PGDMP         
                x            postgres    12.2 (Ubuntu 12.2-4)    12.2 (Ubuntu 12.2-4) H    *           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            +           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            ,           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            -           1262    13503    postgres    DATABASE     z   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE = 'es_ES.UTF-8';
    DROP DATABASE postgres;
                postgres    false            .           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3117                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            /           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1259    17372    cliente    TABLE     �   CREATE TABLE public.cliente (
    id bigint NOT NULL,
    gimnasio_id bigint,
    usuario_id bigint,
    dni integer,
    nombre character varying(100)
);
    DROP TABLE public.cliente;
       public         heap    pr2018    false    3            �            1259    17370    cliente_id_seq    SEQUENCE     �   ALTER TABLE public.cliente ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    215    3            �            1259    17327    gimnasio    TABLE     �   CREATE TABLE public.gimnasio (
    id bigint NOT NULL,
    nombre character varying(100),
    localidad character varying(100)
);
    DROP TABLE public.gimnasio;
       public         heap    pr2018    false    3            �            1259    17325    gimnasio_id_seq    SEQUENCE     �   ALTER TABLE public.gimnasio ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.gimnasio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    205    3            �            1259    17355    gimnasio_producto    TABLE     �   CREATE TABLE public.gimnasio_producto (
    id bigint NOT NULL,
    producto_id bigint,
    gimnasio_id bigint,
    fecha date
);
 %   DROP TABLE public.gimnasio_producto;
       public         heap    pr2018    false    3            �            1259    17353    gimnasio_producto_id_seq    SEQUENCE     �   ALTER TABLE public.gimnasio_producto ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.gimnasio_producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    3    213            �            1259    17430    gimnasio_trabajador    TABLE     �   CREATE TABLE public.gimnasio_trabajador (
    id bigint NOT NULL,
    gimnasio_id bigint,
    trabajador_id bigint,
    fecha date
);
 '   DROP TABLE public.gimnasio_trabajador;
       public         heap    pr2018    false    3            �            1259    17428    gimnasio_trabajador_id_seq    SEQUENCE     �   ALTER TABLE public.gimnasio_trabajador ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.gimnasio_trabajador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    223    3            �            1259    17401    pago    TABLE     l   CREATE TABLE public.pago (
    id bigint NOT NULL,
    cliente_id bigint,
    fecha date,
    monto real
);
    DROP TABLE public.pago;
       public         heap    pr2018    false    3            �            1259    17399    pago_id_seq    SEQUENCE     �   ALTER TABLE public.pago ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.pago_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    219    3            �            1259    17320    producto    TABLE     �   CREATE TABLE public.producto (
    id bigint NOT NULL,
    descripcion character varying(100),
    precio real,
    stock integer
);
    DROP TABLE public.producto;
       public         heap    pr2018    false    3            �            1259    17318    producto_id_seq    SEQUENCE     �   ALTER TABLE public.producto ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    3    203            �            1259    17334 	   proveedor    TABLE     �   CREATE TABLE public.proveedor (
    id bigint NOT NULL,
    nombre character varying(100),
    localidad character varying(100)
);
    DROP TABLE public.proveedor;
       public         heap    pr2018    false    3            �            1259    17332    proveedor_id_seq    SEQUENCE     �   ALTER TABLE public.proveedor ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proveedor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    3    207            �            1259    17413    proveedor_producto    TABLE     �   CREATE TABLE public.proveedor_producto (
    id bigint NOT NULL,
    producto_id bigint,
    proveedor_id bigint,
    fecha date
);
 &   DROP TABLE public.proveedor_producto;
       public         heap    pr2018    false    3            �            1259    17411    proveedor_producto_id_seq    SEQUENCE     �   ALTER TABLE public.proveedor_producto ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proveedor_producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    221    3            �            1259    17341    tipo_trabajador    TABLE     h   CREATE TABLE public.tipo_trabajador (
    id bigint NOT NULL,
    descripcion character varying(100)
);
 #   DROP TABLE public.tipo_trabajador;
       public         heap    pr2018    false    3            �            1259    17339    tipo_trabajador_id_seq    SEQUENCE     �   ALTER TABLE public.tipo_trabajador ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tipo_trabajador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    3    209            �            1259    17389 
   trabajador    TABLE     �   CREATE TABLE public.trabajador (
    id bigint NOT NULL,
    tipo_trabajador_id bigint,
    dni integer,
    nombre character varying(100)
);
    DROP TABLE public.trabajador;
       public         heap    pr2018    false    3            �            1259    17387    trabajador_id_seq    SEQUENCE     �   ALTER TABLE public.trabajador ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.trabajador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    3    217            �            1259    17348    usuario    TABLE     t   CREATE TABLE public.usuario (
    id bigint NOT NULL,
    mail character varying(100),
    "contraseña" integer
);
    DROP TABLE public.usuario;
       public         heap    pr2018    false    3            �            1259    17346    usuario_id_seq    SEQUENCE     �   ALTER TABLE public.usuario ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          pr2018    false    211    3                      0    17372    cliente 
   TABLE DATA                 public          pr2018    false    215                      0    17327    gimnasio 
   TABLE DATA                 public          pr2018    false    205                      0    17355    gimnasio_producto 
   TABLE DATA                 public          pr2018    false    213            '          0    17430    gimnasio_trabajador 
   TABLE DATA                 public          pr2018    false    223            #          0    17401    pago 
   TABLE DATA                 public          pr2018    false    219                      0    17320    producto 
   TABLE DATA                 public          pr2018    false    203                      0    17334 	   proveedor 
   TABLE DATA                 public          pr2018    false    207            %          0    17413    proveedor_producto 
   TABLE DATA                 public          pr2018    false    221                      0    17341    tipo_trabajador 
   TABLE DATA                 public          pr2018    false    209            !          0    17389 
   trabajador 
   TABLE DATA                 public          pr2018    false    217                      0    17348    usuario 
   TABLE DATA                 public          pr2018    false    211            0           0    0    cliente_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.cliente_id_seq', 1, false);
          public          pr2018    false    214            1           0    0    gimnasio_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.gimnasio_id_seq', 1, false);
          public          pr2018    false    204            2           0    0    gimnasio_producto_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.gimnasio_producto_id_seq', 1, false);
          public          pr2018    false    212            3           0    0    gimnasio_trabajador_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.gimnasio_trabajador_id_seq', 1, false);
          public          pr2018    false    222            4           0    0    pago_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.pago_id_seq', 1, false);
          public          pr2018    false    218            5           0    0    producto_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.producto_id_seq', 1, false);
          public          pr2018    false    202            6           0    0    proveedor_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.proveedor_id_seq', 1, false);
          public          pr2018    false    206            7           0    0    proveedor_producto_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.proveedor_producto_id_seq', 1, false);
          public          pr2018    false    220            8           0    0    tipo_trabajador_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.tipo_trabajador_id_seq', 1, false);
          public          pr2018    false    208            9           0    0    trabajador_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.trabajador_id_seq', 1, false);
          public          pr2018    false    216            :           0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.usuario_id_seq', 1, false);
          public          pr2018    false    210            �           2606    17376    cliente cliente_pk 
   CONSTRAINT     P   ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pk PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.cliente DROP CONSTRAINT cliente_pk;
       public            pr2018    false    215            w           2606    17331    gimnasio gimnasio_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.gimnasio
    ADD CONSTRAINT gimnasio_pk PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.gimnasio DROP CONSTRAINT gimnasio_pk;
       public            pr2018    false    205            �           2606    17434 *   gimnasio_trabajador gimnasio_trabajador_pk 
   CONSTRAINT     h   ALTER TABLE ONLY public.gimnasio_trabajador
    ADD CONSTRAINT gimnasio_trabajador_pk PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.gimnasio_trabajador DROP CONSTRAINT gimnasio_trabajador_pk;
       public            pr2018    false    223            �           2606    17405    pago pago_pk 
   CONSTRAINT     J   ALTER TABLE ONLY public.pago
    ADD CONSTRAINT pago_pk PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.pago DROP CONSTRAINT pago_pk;
       public            pr2018    false    219                       2606    17359 &   gimnasio_producto producto_gimnasio_pk 
   CONSTRAINT     d   ALTER TABLE ONLY public.gimnasio_producto
    ADD CONSTRAINT producto_gimnasio_pk PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.gimnasio_producto DROP CONSTRAINT producto_gimnasio_pk;
       public            pr2018    false    213            u           2606    17324    producto producto_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pk PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.producto DROP CONSTRAINT producto_pk;
       public            pr2018    false    203            �           2606    17417 (   proveedor_producto producto_proveedor_pk 
   CONSTRAINT     f   ALTER TABLE ONLY public.proveedor_producto
    ADD CONSTRAINT producto_proveedor_pk PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.proveedor_producto DROP CONSTRAINT producto_proveedor_pk;
       public            pr2018    false    221            y           2606    17338    proveedor proveedor_pk 
   CONSTRAINT     T   ALTER TABLE ONLY public.proveedor
    ADD CONSTRAINT proveedor_pk PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.proveedor DROP CONSTRAINT proveedor_pk;
       public            pr2018    false    207            {           2606    17345 "   tipo_trabajador tipo_trabajador_pk 
   CONSTRAINT     `   ALTER TABLE ONLY public.tipo_trabajador
    ADD CONSTRAINT tipo_trabajador_pk PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.tipo_trabajador DROP CONSTRAINT tipo_trabajador_pk;
       public            pr2018    false    209            �           2606    17393    trabajador trabajador_pk 
   CONSTRAINT     V   ALTER TABLE ONLY public.trabajador
    ADD CONSTRAINT trabajador_pk PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.trabajador DROP CONSTRAINT trabajador_pk;
       public            pr2018    false    217            }           2606    17352    usuario usuario_pk 
   CONSTRAINT     P   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pk;
       public            pr2018    false    211            �           2606    17377    cliente cliente_fk    FK CONSTRAINT     v   ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_fk FOREIGN KEY (usuario_id) REFERENCES public.usuario(id);
 <   ALTER TABLE ONLY public.cliente DROP CONSTRAINT cliente_fk;
       public          pr2018    false    215    2941    211            �           2606    17382    cliente cliente_fk_1    FK CONSTRAINT     z   ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_fk_1 FOREIGN KEY (gimnasio_id) REFERENCES public.gimnasio(id);
 >   ALTER TABLE ONLY public.cliente DROP CONSTRAINT cliente_fk_1;
       public          pr2018    false    215    205    2935            �           2606    17435 *   gimnasio_trabajador gimnasio_trabajador_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.gimnasio_trabajador
    ADD CONSTRAINT gimnasio_trabajador_fk FOREIGN KEY (trabajador_id) REFERENCES public.trabajador(id);
 T   ALTER TABLE ONLY public.gimnasio_trabajador DROP CONSTRAINT gimnasio_trabajador_fk;
       public          pr2018    false    223    217    2947            �           2606    17440 ,   gimnasio_trabajador gimnasio_trabajador_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY public.gimnasio_trabajador
    ADD CONSTRAINT gimnasio_trabajador_fk_1 FOREIGN KEY (gimnasio_id) REFERENCES public.gimnasio(id);
 V   ALTER TABLE ONLY public.gimnasio_trabajador DROP CONSTRAINT gimnasio_trabajador_fk_1;
       public          pr2018    false    2935    223    205            �           2606    17406    pago pago_fk    FK CONSTRAINT     p   ALTER TABLE ONLY public.pago
    ADD CONSTRAINT pago_fk FOREIGN KEY (cliente_id) REFERENCES public.cliente(id);
 6   ALTER TABLE ONLY public.pago DROP CONSTRAINT pago_fk;
       public          pr2018    false    219    2945    215            �           2606    17360 &   gimnasio_producto producto_gimnasio_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.gimnasio_producto
    ADD CONSTRAINT producto_gimnasio_fk FOREIGN KEY (producto_id) REFERENCES public.producto(id);
 P   ALTER TABLE ONLY public.gimnasio_producto DROP CONSTRAINT producto_gimnasio_fk;
       public          pr2018    false    203    2933    213            �           2606    17365 (   gimnasio_producto producto_gimnasio_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY public.gimnasio_producto
    ADD CONSTRAINT producto_gimnasio_fk_1 FOREIGN KEY (gimnasio_id) REFERENCES public.gimnasio(id);
 R   ALTER TABLE ONLY public.gimnasio_producto DROP CONSTRAINT producto_gimnasio_fk_1;
       public          pr2018    false    205    213    2935            �           2606    17418 (   proveedor_producto producto_proveedor_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.proveedor_producto
    ADD CONSTRAINT producto_proveedor_fk FOREIGN KEY (producto_id) REFERENCES public.producto(id);
 R   ALTER TABLE ONLY public.proveedor_producto DROP CONSTRAINT producto_proveedor_fk;
       public          pr2018    false    2933    203    221            �           2606    17423 *   proveedor_producto producto_proveedor_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY public.proveedor_producto
    ADD CONSTRAINT producto_proveedor_fk_1 FOREIGN KEY (proveedor_id) REFERENCES public.proveedor(id);
 T   ALTER TABLE ONLY public.proveedor_producto DROP CONSTRAINT producto_proveedor_fk_1;
       public          pr2018    false    2937    221    207            �           2606    17394    trabajador trabajador_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.trabajador
    ADD CONSTRAINT trabajador_fk FOREIGN KEY (tipo_trabajador_id) REFERENCES public.tipo_trabajador(id);
 B   ALTER TABLE ONLY public.trabajador DROP CONSTRAINT trabajador_fk;
       public          pr2018    false    209    217    2939               
   x���             
   x���             
   x���          '   
   x���          #   
   x���             
   x���             
   x���          %   
   x���             
   x���          !   
   x���             
   x���         