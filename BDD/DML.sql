
INSERT INTO public.gimnasio (razon_social,localidad,direccion,cuit,telefono) VALUES 
('Viking','Moron','Colon 339',20111111112,46978137)
,('Garage gym','Rafael Castillo','Bouchard 440',20222222223,44507515)
;
INSERT INTO public.tipo_trabajador (descripcion) VALUES 
('Normal')
,('Crossfit')
,('Recepcion')
;
INSERT INTO public.trabajador (tipo_trabajador_id,dni,nombre,apellido,genero,telefono,es_activo,direccion,fecha_ingreso,fecha_egreso,mail,fecha_nacimiento) VALUES 
(1,11111111,'Skay','Beilison','M',44517865,true,'aguero 998','2010-03-21',NULL,'skay@gmail.com','1989-04-30')
,(1,22222222,'Carlos','Solari','M',46978238,true,'Maza 4036','2020-04-30',NULL,'carlos@gmail.com','1987-04-30')
,(2,33333333,'Javier','Camelino','M',NULL,true,'Belgrano 315','2020-01-23',NULL,'javier@gmail.com','1989-11-29')
,(2,44444444,'Nahuel','Milessi','M',46979046,true,'Soler 445','2018-07-22',NULL,'nahuel@gmail.com','1987-03-21')
,(3,55555555,'Noelia','Garcia','F',NULL,NULL,'Cabildo 443','2017-09-14','2020-03-21','noelia@gmail.com','1999-08-04')
,(3,66666666,'Yanina','Lopez','F',46978654,true,'Solari 778','2016-09-30',NULL,'yanina@gmail.com','1985-09-08')
;
INSERT INTO public.proveedor (razon_social,localidad,telefono,direccion,mail,es_activo,fecha_ingreso,fecha_egreso) VALUES 
('Casa jorge','Capital Federal',46978967,'Avellaneda 889','jorge@gmail.com',true,'2017-09-21',NULL)
,('Malabia','San miguel',46970987,'Piedra Buena 776','malabia@gmail.com',true,'2019-06-07',NULL)
;
INSERT INTO public.producto (descripcion,precio,stock) VALUES 
('Coca Cola',130.0,20)
,('Gatorade',120.0,50)
,('Aquarius',100.0,40)
;
INSERT INTO public.gimnasio_trabajador (gimnasio_id,trabajador_id) VALUES 
(1,1)
,(1,3)
,(1,5)
,(2,2)
,(2,4)
,(2,6)
,(2,3)
;
INSERT INTO public.proveedor_producto (producto_id,proveedor_id,fecha) VALUES 
(1,1,'2020-03-01')
,(2,1,'2020-03-04')
,(3,1,'2020-04-05')
,(1,2,'2020-05-06')
;
INSERT INTO public.gimnasio_producto (producto_id,gimnasio_id) VALUES 
(1,1)
,(2,1)
,(3,1)
,(1,2)
,(2,2)
,(3,2)
;
INSERT INTO public.usuario (mail,contraseña) VALUES 
('gustavo@gmail.com','1234')
,('agus@gmail.com','1111')
,('cinthia@gmail.com','2222')
,('mariela@gmail.com','3333')
,('micaela@gmail.com','4444')
,('ezequiel@gmail.com','5555')
,('fernanda@gmail.com','6666')
,('gabriela@gmail.com','7777')
,('norma@gmail.com','8888')
,('jose@gmail.com','9999')
;
INSERT INTO public.tipo_cliente (descripcion,monto) VALUES 
('Basico',900)
,('Full',1500)
;
INSERT INTO public.cliente (gimnasio_id,usuario_id,dni,nombre,apellido,fecha_nacimiento,genero,telefono,es_activo,obra_social,fecha_ingreso,fecha_egreso,tipo_cliente_id) VALUES 
(1,1,11112222,'Gustavo','Correa','1989-04-30','M',46978076,true,true,'2020-01-01',NULL,1)
,(1,2,11113333,'Agustin','Espina','1987-12-20','M',NULL,true,true,'2020-01-12',NULL,1)
,(1,3,11114444,'Cinthia','Lopez','2000-06-03','F',46978123,true,true,'2020-01-05',NULL,1)
,(1,4,11115555,'Mariela','Garcia','1986-07-25','F',44509087,true,false,'2020-02-28',NULL,2)
,(1,5,11116666,'Micaela','Perez','1990-09-08','F',44537654,false,true,'2019-01-03','2019-01-21',2)
,(2,7,22228888,'Fernanda','Garcia','1963-10-20','F',NULL,true,true,'2020-06-01',NULL,1)
,(2,8,22229999,'Gabriela','Herrera','1985-09-07','F',NULL,true,true,'2020-07-09',NULL,1)
,(2,9,33331111,'Norma','Aguilera','1989-05-06','F',46978932,true,true,'2020-06-01',NULL,2)
,(2,10,33332222,'Jose','Carro','2001-09-02','M',NULL,true,true,'2020-04-01',NULL,2)
,(1,6,11117777,'Ezequiel','Perez','1980-09-10','M',44568976,true,true,'2019-12-08',NULL,2)
;
INSERT INTO public.pago (cliente_id,fecha,pagado) VALUES 
(1,'2020-02-28',true)
,(2,'2020-02-28',true)
,(3,'2020-02-28',true)
,(4,'2020-02-28',true)
,(4,'2020-03-30',true)
,(10,'2020-04-30',true)
,(10,'2020-05-30',true)
,(6,'2019-12-30',true)
,(6,'2020-01-30',true)
,(1,'2020-01-30',true)
,(2,'2020-01-30',true)
,(3,'2020-01-30',true)
,(7,'2020-01-30',true)
,(7,'2020-02-28',true)
,(8,'2020-07-30',true)
,(9,'2020-06-30',true)
,(9,'2020-07-30',true)
,(5,NULL,false)
;
