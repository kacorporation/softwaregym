-- public.producto definition

-- Drop table

-- DROP TABLE public.producto;

CREATE TABLE public.producto (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	descripcion varchar(100) NULL,
	precio float4 NULL,
	stock int4 NULL,
	CONSTRAINT producto_pk PRIMARY KEY (id)
);


-- public.gimnasio definition

-- Drop table

-- DROP TABLE public.gimnasio;

CREATE TABLE public.gimnasio (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	nombre varchar(100) NULL,
	localidad varchar(100) NULL,
	CONSTRAINT gimnasio_pk PRIMARY KEY (id)
);


-- public.proveedor definition

-- Drop table

-- DROP TABLE public.proveedor;

CREATE TABLE public.proveedor (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	nombre varchar(100) NULL,
	localidad varchar(100) NULL,
	CONSTRAINT proveedor_pk PRIMARY KEY (id)
);


-- public.tipo_trabajador definition

-- Drop table

-- DROP TABLE public.tipo_trabajador;

CREATE TABLE public.tipo_trabajador (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	descripcion varchar(100) NULL,
	CONSTRAINT tipo_trabajador_pk PRIMARY KEY (id)
);


-- public.usuario definition

-- Drop table

-- DROP TABLE public.usuario;

CREATE TABLE public.usuario (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	mail varchar(100) NULL,
	contraseña int4 NULL,
	CONSTRAINT usuario_pk PRIMARY KEY (id)
);


-- public.gimnasio_producto definition

-- Drop table

-- DROP TABLE public.gimnasio_producto;

CREATE TABLE public.gimnasio_producto (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	producto_id int8 NULL,
	gimnasio_id int8 NULL,
	fecha date NULL,
	CONSTRAINT producto_gimnasio_pk PRIMARY KEY (id),
	CONSTRAINT producto_gimnasio_fk FOREIGN KEY (producto_id) REFERENCES producto(id),
	CONSTRAINT producto_gimnasio_fk_1 FOREIGN KEY (gimnasio_id) REFERENCES gimnasio(id)
);


-- public.cliente definition

-- Drop table

-- DROP TABLE public.cliente;

CREATE TABLE public.cliente (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	gimnasio_id int8 NULL,
	usuario_id int8 NULL,
	dni int4 NULL,
	nombre varchar(100) NULL,
	CONSTRAINT cliente_pk PRIMARY KEY (id),
	CONSTRAINT cliente_fk FOREIGN KEY (usuario_id) REFERENCES usuario(id),
	CONSTRAINT cliente_fk_1 FOREIGN KEY (gimnasio_id) REFERENCES gimnasio(id)
);


-- public.trabajador definition

-- Drop table

-- DROP TABLE public.trabajador;

CREATE TABLE public.trabajador (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	tipo_trabajador_id int8 NULL,
	dni int4 NULL,
	nombre varchar(100) NULL,
	CONSTRAINT trabajador_pk PRIMARY KEY (id),
	CONSTRAINT trabajador_fk FOREIGN KEY (tipo_trabajador_id) REFERENCES tipo_trabajador(id)
);


-- public.pago definition

-- Drop table

-- DROP TABLE public.pago;

CREATE TABLE public.pago (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	cliente_id int8 NULL,
	fecha date NULL,
	monto float4 NULL,
	CONSTRAINT pago_pk PRIMARY KEY (id),
	CONSTRAINT pago_fk FOREIGN KEY (cliente_id) REFERENCES cliente(id)
);


-- public.proveedor_producto definition

-- Drop table

-- DROP TABLE public.proveedor_producto;

CREATE TABLE public.proveedor_producto (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	producto_id int8 NULL,
	proveedor_id int8 NULL,
	fecha date NULL,
	CONSTRAINT producto_proveedor_pk PRIMARY KEY (id),
	CONSTRAINT producto_proveedor_fk FOREIGN KEY (producto_id) REFERENCES producto(id),
	CONSTRAINT producto_proveedor_fk_1 FOREIGN KEY (proveedor_id) REFERENCES proveedor(id)
);


-- public.gimnasio_trabajador definition

-- Drop table

-- DROP TABLE public.gimnasio_trabajador;

CREATE TABLE public.gimnasio_trabajador (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	gimnasio_id int8 NULL,
	trabajador_id int8 NULL,
	fecha date NULL,
	CONSTRAINT gimnasio_trabajador_pk PRIMARY KEY (id),
	CONSTRAINT gimnasio_trabajador_fk FOREIGN KEY (trabajador_id) REFERENCES trabajador(id),
	CONSTRAINT gimnasio_trabajador_fk_1 FOREIGN KEY (gimnasio_id) REFERENCES gimnasio(id)
);